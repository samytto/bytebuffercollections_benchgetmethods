package com.metamx.collections.bitmap;

import it.uniroma3.mat.extendedset.intset.ConciseSet;
import it.uniroma3.mat.extendedset.intset.ImmutableConciseSet;

public class BenchmarkGetMethods {

	static int warmup = 1000;
	static int repeat= 100;
	
	public static void main(String[] args) {		
		final ConciseSet cs = new ConciseSet();
		DataGenerator dg = new DataGenerator();
		int[] data = dg.getUniform(0.01);
		System.out.println("data generated. Nb data = "+data.length);
		for(int x : data)
			cs.add(x);
		ImmutableConciseSet ics = new ImmutableConciseSet().newImmutableFromMutable(cs);
		WrappedImmutableConciseBitmap wics = new WrappedImmutableConciseBitmap(ics);
		
		//warming-up		
		for(int i=0; i<warmup; i++){
			int cp=0;
			for(int x : data){
				//wics.get(x);
				wics.get_Intersect(x);
				wics.get_Iterator(x);
				cp++;
				if(cp==data.length/100)
					break;
			}
		}
		System.out.println("warming-up phase finished.");
		//Measuring average running times
		double getTime = 0, getIntersectTime=0, getIteratorTime=0;		
		for(int i=0; i<repeat; i++){
			int cp=0;
			for(int x : data){
				long t=System.currentTimeMillis();
				//wics.get(x);
				getTime+=System.currentTimeMillis()-t;
				t=System.currentTimeMillis();
				wics.get_Iterator(x);
				getIteratorTime+=System.currentTimeMillis()-t;
				t=System.currentTimeMillis();
				wics.get_Intersect(x);
				getIntersectTime+=System.currentTimeMillis()-t;				
				cp++;
				if(cp==data.length/100)
					break;
			}
		}
		System.out.println("Measuring phase finished.");
		
		System.out.println("get avg time = "+getTime/(float)repeat);
		System.out.println("getIntersection avg time = "+getIntersectTime/(float)repeat);
		System.out.println("getIterator avg time = "+getIteratorTime/(float)repeat);
	}
}
